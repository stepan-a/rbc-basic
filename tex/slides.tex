\documentclass[a4,10pt]{seminar}

\usepackage{ccicons}
\usepackage{amssymb}
\usepackage[centertags]{amsmath}
\usepackage{amstext}
\usepackage{amsbsy}
\usepackage{amsopn}
\usepackage{amscd}
\usepackage{amsxtra}
\usepackage{amsthm}
\usepackage{float}
\usepackage{fancyhdr}
\usepackage{pstricks}
\usepackage{pst-all}
\usepackage{graphicx}
\usepackage{semlayer}
\usepackage{color}
\usepackage{mathrsfs}
\usepackage{bm}
\usepackage{lastpage}
\usepackage[nice]{nicefrac}
\usepackage{setspace}
\usepackage[colorlinks=true]{hyperref}
\usepackage[noanswer]{exercise}%noexercise

%\usepackage[landscape]{geometry}

\input{seminar.bug}
\input{seminar.bg2}
\usepackage[utf8]{inputenc}

\overlaysfalse


\newcommand{\trace}{\mathrm{tr}}
\newcommand{\vect}{\mathrm{vec}}
\newcommand{\tracarg}[1]{\mathrm{tr}\left\{#1\right\}}
\newcommand{\vectarg}[1]{\mathrm{vec}\left(#1\right)}
\newcommand{\vecth}[1]{\mathrm{vech}\left(#1\right)}
\newcommand{\iid}[2]{\mathrm{iid}\left(#1,#2\right)}
\newcommand{\normal}[2]{\mathcal N\left(#1,#2\right)}
\newcommand{\dynare}{\textsc{dynare}}
\newcommand{\sample}{\mathcal Y_T}
\newcommand{\samplet}[1]{\mathcal Y_{#1}}
\newcommand{\slidetitle}[1]{\fancyhead[L]{\textsc{#1}}}

\newcommand{\totalnumberofslides}{10}

\setlength\parindent{0pt}

\renewcommand{\headrulewidth}{0.2mm}
\renewcommand{\footrulewidth}{0.2mm}

\newenvironment{notes}{\begin{note}\setlength{\parskip}{0.1cm}\tiny\begin{spacing}{.9}}{\end{spacing}\end{note}}

\renewcommand{\ExerciseHeader}{{%
\textbf{\ExerciseHeaderDifficulty\ExerciseName\ %
\ExerciseHeaderNB\ExerciseHeaderTitle\ExerciseHeaderOrigin}}\medskip}

\newcommand{\ladate}{September, 2013}

\definecolor{gris25}{gray}{0.75}

\def\printlandscape{\special{landscape}}
\makeatletter
  \def\pst@initoverlay#1{%
  \pst@Verb{%
  /BeginOL {dup (all) eq exch TheOL le or {IfVisible not {Visible
  /IfVisible true def} if} {IfVisible {Invisible /IfVisible false
  def} if} ifelse} def \tx@InitOL /TheOL (#1) def}} \makeatother


% ----------------------------------------------------------------
% Slides *********************************************************
% ----------------------------------------------------------------

\slideframe{none}


\fancyhf{}
\fancyhead{ }
\fancyfoot[L]{\tiny S. Adjemian \href{http://creativecommons.org/licenses/by-sa/3.0/legalcode}{\ccbysa}\hspace{.1cm}\raisebox{-.05cm}{\href{https://bitbucket.org/stepan-a/rbc-basic.git}{\includegraphics[scale=.05]{../img/bitbucket.png}}}}
\fancyfoot[C]{\tiny Basic Real Business Cycle model}
\fancyfoot[R]{\tiny \ladate\ -- \theslide/\totalnumberofslides}


%\autoslidemarginstrue
\centerslidesfalse




\begin{document}

\pagestyle{fancy}



\begin{slide}
\title{\texttt{Basic Real Business Cycle model}\\ \small{\texttt{a canonical Dynamic Stochastic General Equilibrium model}}}
\author{\texttt{Stéphane Adjemian}\\ \texttt{Université du Maine, Gains \& Cepremap}\\
\textmd{\texttt{stephane.adjemian@univ-lemans.fr}}\\}
\date{\texttt{\ladate}}
 \maketitle
\end{slide}


\begin{slide}
  \slidetitle{Statement of the Central Planner's Problem}
  \begin{equation}
    \label{eq:central_planner_program}
    \begin{split}
      &\max_{\{c_{t+j},k_{t+1+j}\}_{j=0}^{\infty}} \mathbb E_t\left[\sum_{j=0}^\infty\beta^j\log c_{t+j}\right]\\
     s.t.\qquad c_{t+j} + k_{t+1+j} &= e^{a_{t+j}} k_{t+j}^{\alpha} + (1-\delta)k_{t+j}\quad\forall j\geq 0\\
     a_t &= \rho a_{t-1} + \sigma\varepsilon_t,\quad \text{with}\, \varepsilon_{t} \underset{\textit{iid}}{\sim} \mathcal N(0,1),\quad \forall t
    \end{split}
  \end{equation}
  \begin{itemize}
  \item $\beta\in(0,1)$ is the discount factor,
  \item $\alpha\in(0,1)$ is the elasticity of production with respect to physical capital,
  \item $\delta\in(0,1)$ is the depreciation rate of physical capital stock,
  \item Autoregressive parameter $\rho$ is assumed to be less than one in absolute value,
  \item $\sigma$ is a positive scale parameter
  \end{itemize}
\end{slide}

\begin{notes}
    $\bullet$ For simplicity, we state the central planner formulation of the problem. We know that, without any departure from the perfect competition assumptions, we would obtain exactly the same equilibrium dynamics by stating the decentralized problem.\newline


    $\bullet$ Usual notations: $c_t$, $k_t$ and $a_t$ respectively stand for consumption, physical capital stock and (logged) exogenous efficiency.\newline

    $\bullet$ The symbol $\mathbb E_t$ represents the mathematical expectation conditional on the available information at time $t$, which will be denoted $\mathscr F_t$ in the sequel. For the class of models we will deal with in this course the information set $\mathscr F_t$ can be replaced without loss of information, when computing conditional moments, by the current levels of a subset of variables: the so called state variables. We don't need to keep track of the entire history (which would be infeasible). In our basic RBC model, the state variables at time $t$ are the current levels of  physical capital stock, $k_t$,  and efficiency, $a_t$. For instance, we have:
\[
\begin{split}
\mathbb E_t \left[a_{t+1}\right] &= \mathbb E\left[a_{t+1}\biggl|\mathscr F_t\right]\\
&= \mathbb E\left[\rho a_{t} + \sigma\varepsilon_{t+1}\biggl|\mathscr F_t\right]\text{ (by substituting the definition of the exogenous  process)}\\
&= \mathbb E\left[\rho a_{t} \biggl|\mathscr F_t\right] + \sigma\mathbb E\left[\varepsilon_{t+1}\biggl|\mathscr F_t\right]\text{ (by linearity of the mathematical expectation)}\\
&= \mathbb \rho a_{t} + \sigma\mathbb E\left[\varepsilon_{t+1}\biggl|\mathscr F_t\right]\text{ (because $a_t\in\mathscr F_t$)}\\
&= \mathcal \rho a_{t}\text{ because $\varepsilon_t$ is a strong white noise}.
\end{split}
\]
To compute the conditional expectation of $a_{t+1}$ we only need to know the state of logged efficiency, $a_t$, and the Data Generating Process (DGP) of logged efficiency. The same is true if we need to compute the conditional mathematical expectation of a nonlinear transformation of $a_{t+1}$. For instance, we have:
\[
\begin{split}
\mathbb E_t \left[e^{a_{t+1}}\right] &= \mathbb E\left[\left(e^{a_{t}}\right)^{\rho}e^{\varepsilon_{t+1}}\biggl|\mathscr F_t\right]\\
&= \left(e^{a_{t}}\right)^{\rho}\mathbb E\left[e^{\varepsilon_{t+1}}\biggl|\mathscr F_t\right]\text{ (because $a_t\in\mathscr F_t$)}\\
&= \left(e^{a_{t}}\right)^{\rho}e^{\frac{\sigma^2}{2}}\text{ by definition of the log-normal distribution}.
\end{split}
\]
A definition of the log-normal distribution can be found on \href{http://en.wikipedia.org/wiki/Log-normal_distribution}{wikipedia.org}. It can be easily established that the conditional mode is also affected by the variance. The mode of a log-normal random variable is a decreasing function of the variance while its mean is an increasing. The conditional median is invariant with respect to the variance (the conditional median of efficiency is equal to one).
\end{notes}

\begin{slide}
  \slidetitle{Optimal allocation (I, Lagrangian)}
  The Lagrangian associated to this problem is given by:
  {\small
  \[
  \mathscr L = \mathbb E_t \left[\sum_{j=0}^{\infty} \beta^j \left(\log c_{t+j} + \lambda_{t+j} \left\{e^{a_{t+j}} k_{t+j}^{\alpha} + (1-\delta)k_{t+j} - c_{t+j} - k_{t+1+j}\right\}\right)\right]
  \]
  }
  where $\lambda_{t+j}\geq 0$ is the Lagrange multiplier for the resource constraint.\newline

  The dynamics of consumption and physical capital are characterized by the following first order conditions:
  \begin{equation}
    \label{eq:foc:1}\tag{FOC.1}
    \frac{1}{c_t} = \beta\mathbb E_t \left[\frac{1}{c_{t+1}}\left(\alpha e^{a_{t+1}} k_{t+1}^{\alpha-1}+1-\delta\right)\right]
  \end{equation}
  \begin{equation}
    \label{eq:foc:2}\tag{FOC.2}
    c_t + k_{t+1} = e^{a_t} k_t^{\alpha} + (1-\delta)k_t
  \end{equation}
  for all $t\geq 0$.
\end{slide}


\begin{notes}
  $\bullet$ The Lagrangian can be alternatively written in the following manner:
  \[
  \begin{split}
    \mathscr L = \mathbb E_t \Biggl[ \log {\color{blue}c_t} + \lambda_t \Bigl\{e^{a_t} k_t^{\alpha} &+ (1-\delta)k_t - {\color{blue}c_t} - {\color{red}k_{t+1}}\Bigr\}\\
                                    \beta\Bigl(\log c_{t+1} &+ \lambda_{t+1} \Bigl\{e^{a_{t+1}} {\color{red}k_{t+1}^{\color{red}\alpha}} + (1-\delta){\color{red}k_{t+1}} - c_{t+1} - k_{t+2}\Bigr\}\Bigr)\\
                                    &+ \dots\\
                                    \beta^{s} \Bigl(\log c_{s} &+ \lambda_{s} \Bigl\{e^{a_{s}} k_{s}^{\alpha} + (1-\delta)k_{s} - c_{s} - k_{s+1}\Bigr\}\Bigr)\\
                                    &+ \dots \Biggr]
  \end{split}
  \]
  The first order conditions are obtained by setting to zero the first partial derivatives of $\mathscr L$ with respect to ${\color{blue}c_{t}}$ and ${\color{red}k_{t+1}}$. We obtain:
  \begin{equation}
    \label{eq:lagrangian:note:foc1}\tag{a}
     \lambda_t = \frac{1}{c_t}
  \end{equation}
  and
  \begin{equation}
    \label{eq:lagrangian:note:foc2}\tag{b}
    \lambda_t = \beta\mathbb E_t\Bigl[\lambda_{t+1}\left(\alpha e^{a_{t+1}}k_{t+1}^{\alpha-1}+1-\delta\right)\Bigr]
  \end{equation}
  Substituting (\ref{eq:lagrangian:note:foc1}) into (\ref{eq:lagrangian:note:foc2}) we get the Euler equation (\ref{eq:foc:1}).\newline

  $\bullet$ The Lagrange multiplier, $\lambda_t$, is also called the shadow price of capital. $\lambda_t$ says how much the household is willing to pay for an additional unit of physical capital tomorrow. Condition (\ref{eq:lagrangian:note:foc1}) relates this implicit price to the marginal utility of consumption (an additional unit of physical capital tomorrow is at the cost of one unit of consumption today).\newline

  $\bullet$ Condition (\ref{eq:lagrangian:note:foc2}), the Euler equation, describes the dynamic of the Lagrange multiplier. This equation can be rearranged as follows:
  \[
  1 = \mathbb E_t \Biggl[\underbrace{\beta \frac{\lambda_{t+1}}{\lambda_t}}_{\text{\parbox{3cm}{\centering Stochastic discount\\ factor}}} \times
  \underbrace{\left(1+\alpha e^{a_{t+1}}k_{t+1}^{\alpha-1}-\delta\right)}_{\text{\parbox{3cm}{\centering Future real gross return\\ to physical capital}}}\Biggr]
  \]
  At the optimum, the expected discounted gross return of physical capital net from the depreciation has to be equal to one. This condition is quite intuitive. If the expected discounted rate of return is negative the welfare can be improved by lowering $k_{t+1}$ (or by complementarity increasing the current level of consumption $c_t$) because the marginal productivity of capital is a decreasing function of the physical capital stock.

\end{notes}


\begin{slide}
  \slidetitle{Optimal allocation (II, Dynamic programming -- \textnormal{a} --)}

  Define the value function (we omit the DGP for $a_t$):
  \begin{equation}
    \label{eq:dynamic_problem}
  \begin{split}
    \mathscr V(k_t,a_t) = &\max_{\{c_{t+j},k_{t+1+j}\}_{j=0}^{\infty}} \mathbb E_t\left[\sum_{j=0}^\infty\beta^j\log c_{t+j}\right]\\
     s.t.\qquad c_{t+j} + k_{t+1+j} &= e^{a_{t+j}} k_{t+j}^{\alpha} + (1-\delta)k_{t+j}\quad\forall j\geq 0
  \end{split}
  \end{equation}
  If a (time invariant) solution to this problem exists, then we can recursively restate the problem as:
  \begin{equation}
    \label{eq:bellman_equation}
  \begin{split}
    \mathscr V(k_t,a_t) = &\max_{\{c_{t},k_{t+1}\}} \log c_t +\beta \mathbb E_t\left[\mathscr V(k_{t+1},a_{t+1})\right]\\
     &s.t.\qquad c_{t} + k_{t+1} = e^{a_{t}} k_{t}^{\alpha} + (1-\delta)k_{t}
  \end{split}
  \end{equation}
  Problems (\ref{eq:dynamic_problem}) and (\ref{eq:bellman_equation}) are equivalent under weak conditions (plus a transversality condition ensuring that the conditional expectation of $\beta^j\mathscr V(k_{t+j},a_{t+j})$ goes to zero as $j$ tends to infinity).

\end{slide}


\begin{slide}
  \slidetitle{Optimal allocation (II, Dynamic programming -- \textnormal{b} --)}

  By restating problem (\ref{eq:dynamic_problem}) as (\ref{eq:bellman_equation}), called the Bellman equation, we transform an infinite horizon problem into a two periods optimization problem!\newline

  One might think that (\ref{eq:bellman_equation}) is an impossible problem to solve, because the unknown value $\mathscr V$ appears in the expression of the function to be optimized. We will see that we can get rid of this value function by using the envelop theorem.\newline

\end{slide}


\begin{slide}
  \slidetitle{Envelop theorem}

  Define the following (static) optimization problem:
  \[
  \begin{split}
    \max_{\bm x} &f(\bm x, \bm a)\\
    \text{s.t. }& g(\bm x, \bm a) = 0
  \end{split}
  \]
  where $f$ and $g$ are continuous functions such that a solution exists, $\bm x$ is a vector of variables and $\bm a$ is a vector of parameters.\newline

  Let $\bm x^{\star}(\bm a)$ be the solution and $f^{\star}(\bm a) = f(x^{\star}(\bm a))$ the optimal value.\newline

  Define the Lagrangian: $ \mathcal L (\bm x, \bm a) = f(\bm x, \bm a) + \bm \lambda \cdot g(\bm x, \bm a)$\newline

\[
\hookrightarrow\qquad
\frac{\mathrm d f^{\star}(\bm a)}{\mathrm d a_i} = \frac{\partial \mathcal L (\bm x, \bm a)}{\partial a_i}\Bigg|_{\bm x = \bm x^{\star}(\bm a), \bm \lambda = \bm \lambda^{\star}(\bm a)}
\]

\end{slide}

\begin{slide}
  \slidetitle{Optimal allocation (II, Dynamic programming -- \textnormal{c} --)}

  Substituting the resource constraint in the objective (if this not possible, we can write a Lagrangian), we clearly see that we just need to optimize with respect $k_{t+1}$. The first order condition is given by:
  \[
  \frac{1}{c_t} = \beta \mathbb E_t \left[\frac{\partial \mathscr V (k_{t+1},a_{t+1})}{\partial k_{t+1}}\right]
  \]

  This condition contains a partial derivative of an unknown function (the value $\mathscr V$). Using the envelop theorem, we have:
  \[
  \frac{\partial \mathscr V (k_{t},a_{t})}{\partial k_{t}} = \frac{\alpha e^{a_t}k_t^{\alpha-1}+1-\delta}{c_t}
  \]
  Iterating forward and substituting in the first order condition we obtain:
  \[
  \frac{1}{c_t} = \beta \mathbb E_t \left[\frac{1}{c_{t+1}}\left(\alpha e^{a_{t+1}}k_{t+1}^{\alpha-1}+1-\delta\right)\right]
  \]
  which turns out to be the Euler equation!

\end{slide}

\begin{slide}
  \slidetitle{Closed form solution ($\delta=1$)}

  In the unrealistic case where the depreciation rate, $\delta$, is equal to one (the physical capital stock used in $t+1$ is the time $t$ investment) we can provide an analytical solution for this model.\newline

  $\rightarrow$ Express the control variables ($c_t$ and/or $k_{t+1}$) as an invariant functions of the states ($k_t$ and $a_t$).\newline

  We postulate that the propensity to consume (or the saving rate) is constant: $c_t = A y_t = A e^{a_t}k_t^{\alpha}$ for all $t$. One can show by identification that:
  \[
  c_t = (1-\alpha\beta)e^{a_t}k_t^{\alpha} \text{ and } k_{t+1} = \alpha\beta e^{a_t}k_t^{\alpha} \text{ for all }t
  \]
\end{slide}


\begin{notes}
  $\bullet$ We identify the solution of the model using the undetermined coefficients approach. Substituting the postulated solution in (\ref{eq:foc:1}) and (\ref{eq:foc:2}) we obtain:
  \[
    \frac{1}{A e^{a_t}k_t^{\alpha}} = \beta\mathbb E_t \left[\frac{1}{A e^{a_{t+1}}k_{t+1}^{\alpha}}\left(\alpha e^{a_{t+1}} k_{t+1}^{\alpha-1}+\underbrace{1-\delta}_{=0}\right)\right]
  \]
  and
  \[
    Ae^{a_t} k_t^{\alpha} + k_{t+1} = e^{a_t} k_t^{\alpha} +\underbrace{(1-\delta)k_t}_{=0}
  \]
  or equivalently:
  \[
  \begin{cases}
    \frac{1}{A e^{a_t}k_t^{\alpha}} &= \beta\mathbb E_t \left[\frac{\alpha}{A}k_{t+1}^{-1}\right]\\
    k_{t+1} &= (1-A)e^{a_t} k_t^{\alpha}
  \end{cases}
  \]
  Substituting the second equation in the first one and simplifying for $A$, we have:
  \[
  \frac{1}{e^{a_t}k_t^{\alpha}} = \beta\alpha \mathbb E_t \left[\left((1-A)e^{a_t} k_t^{\alpha}\right)^{-1}\right]
  \]
  Because $a_t$ and $k_t$ belongs to the information set $\mathscr F_t$, we can safely remove the conditional expectation:
  \[
  \frac{1}{e^{a_t}k_t^{\alpha}} = \beta\alpha \left((1-A)e^{a_t} k_t^{\alpha}\right)^{-1}
  \]
  The production, $y_t=e^{a_t}k_t^{\alpha}$, appearing on both sides cancels out and we finally obtain:
  \[
  A = 1-\alpha\beta
  \]
  So that the consumption and capital levels are given by:
  \[
  c_t = (1-\alpha\beta)e^{a_t}k_t^{\alpha} \text{ and } k_{t+1} = \alpha\beta e^{a_t}k_t^{\alpha}
  \]

  \begin{Exercise}
    Try to follow the same steps when $\delta<1$.
  \end{Exercise}

  \begin{Exercise}
    Consider an extension of this model with endogenous labor supply..
  \end{Exercise}

  $\bullet$ \textbf{Why did we (successfully) postulate a linear solution?} Because I know the solution of the deterministic version of this model! The deterministic Euler equation is:
  \[
  \frac{1}{c_t} = \alpha\beta\frac{k_{t+1}^{\alpha-1}}{c_{t+1}}
  \]
  \[
  \Leftrightarrow \frac{k_{t+1}}{c_t} = \alpha\beta\frac{k_{t+1}^{\alpha}}{c_{t+1}}
  \]
  Substituting the physical capital law of motion yields:
  \[
  \frac{k_{t}^{\alpha}-c_t}{c_t} = \alpha\beta\frac{k_{t+1}^{\alpha}}{c_{t+1}}
  \]
  \[
  \Leftrightarrow \frac{k_{t}^{\alpha}}{c_t} = 1+\alpha\beta\frac{k_{t+1}^{\alpha}}{c_{t+1}}
  \]
  Iterating forward, we obtain:
  \[
  \frac{k_{t}^{\alpha}}{c_t} = 1+\alpha\beta+(\alpha\beta)^2+(\alpha\beta)^3+\cdots(\alpha\beta)^h\frac{k_{t+h}^{\alpha}}{c_{t+h}}
  \]
  Provided that $k_{\infty}$ and $c_{\infty}$ are finite (why?), and $\alpha\beta<1$, we have:
  \[
  \frac{k_{t}^{\alpha}}{c_t} = \frac{1}{1-\alpha\beta}
  \]
  or
  \[
  c_t = (1-\alpha\beta)k_t^{\alpha}
  \]
\end{notes}

\begin{slide}
  \slidetitle{Statistical properties of the closed form solution ($k_t$) -- \textnormal{a} --}

  According the closed form solution we have:
  \[
  \log k_{t} = \log \alpha\beta + \alpha\log k_{t-1} + a_{t-1}
  \]
  Using the lag operator and substituting the DGP for logged efficiency, we also have that:
  \[
  (1 - \alpha L)\log k_{t} = \log \alpha\beta + \frac{\sigma\varepsilon_{t-1}}{1-\rho L}
  \]
  Multiplying by the lag-polynomial $1-\rho L$ on both sides, we get:
  \[
(1-\rho L)(1 - \alpha L)\log k_{t} = (1-\rho)\log \alpha\beta + \sigma \epsilon_{t-1}
  \]
  Developing the lag-polynomial we finally obtain:
  \begin{equation}
    \label{eq:logk:ar2}
  \log k_t = (1-\rho)\log\alpha\beta + (\alpha+\rho)\log k_{t-1} - \alpha\rho \log k_{t-2} + \sigma\varepsilon_{t-1}
  \end{equation}
  The logged capital stock is a second order autoregressive process.
\end{slide}

\begin{notes}

  $\bullet$ The lag operator $L$ is used to shift backward a variable: for any variable $X_t$ we have $LX_t = X_{t-1}$. For more details on this operator and about ARMA models, Gourieroux and Monfort (1997, \textit{Time Series and Dynamic Models}, Cambridge University Press) or Hamilton (1994, \textit{Time Series Analysis}, Princeton University Press) are useful references. \newline

  $\bullet$  Note that $\varepsilon_{t-1} \perp \log k_{t-1}$ (and, {\it a fortiori}, $\varepsilon_{t-1} \perp \log k_{t-2}$) because, at time $t-1$, $k_{t-1}$ is a predetermined variable (the level of the physical capital stock used at time $t-1$ is decided at time $t-2$). This explains why the stochastic process in (\ref{eq:logk:ar2}) is an AR(2) even though the timing of the innovation looks weird (compared to what we may read in time series textbooks).\newline

  $\bullet$ Because the innovation of logged efficiency is Gaussian, the logarithm of the physical capital stock turns out to be Gaussian. This stochastic process is (asymptotically) stationary because the roots of the lag polynomial, $\alpha$ and $\rho$, are both less than one in absolute value.\newline

  $\bullet$ Knowing that $\log k_t \sim AR(2)$ we can easily compute first and second order moments for this endogenous variable. For instance, the unconditional expectation is $\mathbb E\left[\log k_t\right] = \nicefrac{\log \alpha\beta}{1-\alpha}$.\newline

  \begin{Exercise}
    Establish that the unconditional variance of $\log k_t$ is given by:
    \[
    \mathbb V \left[\log k_t\right] = \frac{(1+\alpha\rho)\sigma^2_{\varepsilon}}{(1-\alpha\beta)(1-\alpha)(1-\beta)(1+\alpha)\beta}
    \]\qed
  \end{Exercise}

\end{notes}


\begin{slide}
  \slidetitle{Statistical properties of the closed form solution ($y_t$) -- \textnormal{a} --}
  
  By definition, the logarithm of production is $a_t + \alpha\log k_t$. Knowing that $\log k_t \sim AR(2)$ and that $a_t \sim AR(1)$, it can be shown that
  logged production is an AR(2) stationary Gaussian stochastic process:
  {\small
  \begin{equation}
    \label{eq:logy:ar2}
    \log y_t = \alpha(1-\rho)\log\alpha\beta + (\alpha+\rho)\log y_{t-1} - \alpha\rho\log y_{t-2} + \sigma\varepsilon_t
  \end{equation}}

  The logged production is a Gaussian stationary stochastic process, with expectation $\mu_y$ and variance $\sigma_y^2$\newline

  \begin{Exercise}
    Establish that $\sigma_y^2$ is equal to the unconditional variance of $\log k_t$ and that $\mu_y = \frac{\alpha}{1-\alpha}\log\alpha\beta$.
  \end{Exercise}

\end{slide}

\begin{notes}
  $\bullet$ \textbf{Proof of (\ref{eq:logy:ar2}).} By definition, we have:
  \[
  \log y_t = a_t + \alpha \log k_t
  \]
  Substituting the definition of logged productivity and the solution for $\log k_t$, yields
  \[
  \log y_t = \frac{\sigma\varepsilon_t}{1-\rho L} + \alpha \frac{(1-\rho)\log\alpha\beta+\sigma\varepsilon_{t-1}}{(1-\rho L)(1-\alpha L)}
  \]
  or equivalently:
  \[
  \log y_t = \frac{(1-\alpha L)\sigma\varepsilon_t+\alpha(1-\rho)\log\alpha\beta+\alpha\sigma\varepsilon_{t-1}}{(1-\rho L)(1-\alpha L)}
  \]
  Noting that $\varepsilon_t$s cancel out, we finally obtain:
  \[
  \log y_t = \alpha(1-\rho)\log\alpha\beta + (\alpha+\rho)\log y_{t-1} - \alpha\rho\log y_{t-2} + \sigma\varepsilon_t
  \]

\end{notes}


\begin{slide}
  \slidetitle{Statistical properties of the closed form solution ($y_t$) -- \textnormal{b} --}
  
  The production in level, $y_t$, is a log normal random variable. We have:
  \[
  \mathbb E \left[y_t\right] = e^{\mu_y+\frac{1}{2}\sigma_y^2} = (\alpha\beta)^{\frac{\alpha}{(1-\alpha)}}e^{\frac{1}{2}\sigma_y^2}
  \]
  \[
  \mathbb V \left[y_t\right] = \left(e^{\sigma_y^2}-1\right)e^{2\mu_y+\sigma_y^2}
  \]
  The mode of the unconditional distribution of $y_t$ is inferior to its expectation (the mode is $e^{\mu_y-\sigma_y^2}$). The median of the unconditional distribution of $y_t$ is smaller than its expectation (the median is $(\alpha\beta)^{\frac{\alpha}{(1-\alpha)}}$). We have: mode($y_t$)$<$median($y_t$)$<\mathbb E \left[y_t\right]$.
  \begin{Exercise}
    What is $(\alpha\beta)^{\frac{\alpha}{(1-\alpha)}}$ in this model? \textbf{Hint:} Consider the deterministic version of the model.
  \end{Exercise}


\end{slide}



\end{document}




\end{document}